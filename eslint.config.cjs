const vue = require("eslint-plugin-vue");

module.exports = [
  {
    ignores: ["node_modules/**"],
  },
  {
    files: ["**/*.js", "**/*.vue"],
    languageOptions: {
      ecmaVersion: 2020,
      sourceType: "module",
    },
    plugins: {
      vue,
    },
    rules: {
    },
  },
  {
    plugins: {
      vue,
    },
    rules: {
      ...vue.configs["vue3-recommended"].rules,
    },
  },
];
