<div align="center">

  # Ignisus Progressive Web App 

[![pipeline status](https://gitlab.com/ignisus/hera/badges/main/pipeline.svg)](https://gitlab.com/ignisus/hera/-/commits/main)
[![Latest Release](https://gitlab.com/ignisus/hera/-/badges/release.svg)](https://gitlab.com/ignisus/hera/-/releases)

[![VUE](https://img.shields.io/badge/Vue.js-35495E?style=for-the-badge&logo=vue.js&logoColor=4FC08D)](https://vuejs.org/)
[![VITE](https://img.shields.io/badge/Vite-B73BFE?style=for-the-badge&logo=vite&logoColor=FFD62E)](https://vitejs.dev/)
[![DOCKER](https://img.shields.io/badge/Docker-2CA5E0?style=for-the-badge&logo=docker&logoColor=white)](https://docker.com/)

## [PREVIEW](https://ignisus.org)

</div>

### 📃 Index

1. [Setup](#setup) <br>
   1.1 [Linux setup](#linux) <br>
   1.2 [Mac setup](#mac) <br>
   1.3. [Windows setup](#windows) <br>
2. [Database](#database) <br>
  2.1 [Mongo DB](#mongodb) <br>
3. [Wiki](#wikis) <br>
   3.1 [Commits](#commits) <br>
   3.2 [StoryBook](#storybook) <br>
4. [Acknowledgments](#acknowledgments)

# 💡 Setup <a name="setup"></a>

## 🐧 GNU Linux setup (Flatpak compatible) <a name="linux"></a>

### 📌 Visual Code Flatpak System Terminal Fix:

edit settings.json (you can choose between bash or zsh) and add:

```json
  "terminal.integrated.defaultProfile.linux": "bash",
  "terminal.integrated.profiles.linux": {
    "bash": {
      "path": "/usr/bin/flatpak-spawn",
      "args": [
        "--env=TERM=xterm-256color",
        "--host",
        "script",
        "--quiet",
        "/dev/null"
      ]
    },
    "zsh": {
      "path": "/usr/bin/flatpak-spawn",
      "args": [
        "--env=TERM=vscode",
        "--env=SHELL=zsh",
        "--host",
        "script",
        "--quiet",
        "/dev/null"
      ]
    }
  }
```

### 📌 Nvm install and setup:

Recommend install linux dependences like make, debian example: `apt install build-essential`, fedora example: `sudo dnf groupinstall "Development Tools"` / `rpm-ostree install make`.

Install or update nvm: [Link](https://github.com/nvm-sh/nvm?tab=readme-ov-file#about)

Run `nvm install --lts` get lastest node LTS.

Run `nvm use --lts` for set lastest node LTS.

Alternative, you can install node on flatpak terminal `flatpak search org.freedesktop.Sdk.Extension.node`

### 📌 Vue 3 in Vite install

#### Recommended IDE Setup

Before starting, it is important to read and understand the configuration files and extensions in `.vscode/`.

The HTML attributes will be in multiline.

ESLint for formatting javascript files.

Setup with Composition API + Pinia for state management.

#### Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

#### Project Setup

```bash
npm install
```

#### Project Lint & Unit Tests with [Vitest](https://vitest.dev/)

```bash
npm run format
```

```bash
npm run test:unit
```

#### Local server with Hot-Reload for development

```bash
npm run dev
```

#### Preview

```bash
npm run preview
```

#### Lint with [ESLint](https://eslint.org/)

```bash
npm run lint
```

### 📌 Compile: <a name="compile"></a>

#### Compile and minify for production

```bash
npm run build
```

```bash
tar -czvf public.tar.gz -C public .
```

#### Compile to Linux, Windows and Mac

Install rust rsing rustup (Recommended):

```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

Reload and check the installed version:

```bash
source $HOME/.cargo/env && cargo --version
```

Reconfigure the variables for the build only if you need:

```bash
npx tauri init --force
```

```bash
npm run tauri dev
```

```bash
npx tauri dev
```

To build, may need fix permission on src-tauri with: `sudo chown -XX user:group /path/to/src-tauri/*`

```bash
npx tauri build
```

## 🍎 Mac setup <a name="mac"></a>

Install lastest LTS of node.js and setup Vue 3 + Vite.

Recommend using nvm [NVM for Mac Os](https://github.com/nvm-sh/nvm#troubleshooting-on-macos)

From here you can follow the [Linux guide](#linux) to continue.

## 🪟 Windows setup <a name="windows"></a>

Install lastest LTS of node.js and setup Vue 3 + Vite.

Recommend using nvm [NVM for Windows](https://github.com/coreybutler/nvm-windows)

Recommend using WSL [WSL for Windows](https://learn.microsoft.com/en-us/windows/wsl/install)

From here you can follow the [Linux guide](#linux) to continue.

# 📑 Database: <a name="database"></a>

The first step is install Docker on Linux.

## 📌 Mongo DB <a name="mongodb"></a>

```bash
docker pull mongo
```

```bash
docker run -d --name ignisus -p 27017:27017 mongo
```

```bash
docker container ls
```

```bash
docker exec -it ignisus mongosh
```

And import all `src/services/*.json`

# 📑 Wiki: <a name="wiki"></a>

 📌 <a name="commits">[Commits](https://gitlab.com/ignisus/frontend/-/wikis/home)</a>

 📌 <a name="storybook">[StoryBook](https://gitlab.com/ignisus/frontend/-/wikis/Storybook)</a>

# 🫂 Acknowledgments: <a name="acknowledgments"></a>