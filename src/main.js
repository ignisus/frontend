import './style.css';
import { createApp } from 'vue';
import { createPinia } from 'pinia'
import App from '@/App.vue';
import router from '@/router';
import { createI18n } from 'vue-i18n';
import { languages } from '@/locales';

let savedLanguage = localStorage.getItem('language');
if (!savedLanguage) {
  savedLanguage = 'en';
  localStorage.setItem('language', savedLanguage);
}

let savedTheme = localStorage.getItem('theme');
if (!savedTheme) {
  savedTheme = 'adwaita-dark';
  localStorage.setItem('theme', savedTheme);
}

document.body.className = savedTheme;

const i18n = createI18n({
  locale: savedLanguage,
  fallbackLocale: 'en',
  legacy: false,
  globalInjection: true,
  messages: languages
});

const pinia = createPinia()
const app = createApp(App);
app.use(pinia)
app.use(router);
app.use(i18n);
app.mount('#app');
