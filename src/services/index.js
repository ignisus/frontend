import users from './users.json';
import posts from './posts.json';
import hosting from './hosting.json';
import games from './games.json';
import minecraft from './minecraft.json';

posts.reverse();

export const db = {
  users: users,
  posts: posts,
  hosting: hosting,
  games: games,
  vanilla: minecraft.vanilla,
  mods: minecraft.mods,
};
