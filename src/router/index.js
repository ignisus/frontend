import { createRouter, createWebHistory } from 'vue-router';
import MainView from '@/views/MainView.vue';
import AiView from '@/views/AiView.vue';
import ForumView from '@/views/ForumView.vue';
import PostsDetail from '@/components/PostsDetail.vue'
import MastodonDetail from '@/components/MastodonDetail.vue'
import GamingView from '@/views/GamingView.vue';
import MinecraftView from '@/views/MinecraftView.vue';
import HostingView from '@/views/HostingView.vue';
import SettingsView from '@/views/SettingsView.vue';
import PolicyView from '@/views/PolicyView.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: MainView
    },
    {
      path: '/login',
      name: 'login',
      component: MainView
    },
    {
      path: '/ai',
      name: 'ai',
      component: AiView
    },
    {
      path: '/forum',
      name: 'forum',
      component: ForumView
    },
    {
      path: '/forum/:id',
      name: 'PostsDetail',
      component: PostsDetail,
      props: true
    },
    {
      path: '/forum/post/:id',
      name: 'MastodonDetail',
      component: MastodonDetail,
      props: true
    },
    {
      path: '/create',
      name: 'create',
      component: ForumView
    },
    {
      path: '/gaming',
      name: 'gaming',
      component: GamingView
    },
    {
      path: '/minecraft',
      name: 'minecraft',
      component: MinecraftView
    },
    {
      path: '/hosting',
      name: 'hosting',
      component: HostingView
    },
    {
      path: '/settings',
      name: 'settings',
      component: SettingsView
    },
    {
      path: '/policy',
      name: 'policy',
      component: PolicyView
    }
  ]
});

export default router;
