import es from './es.json';
import en from './en.json';
import fr from './fr.json';

export const languages = {
  es: es,
  en: en,
  fr: fr
};
