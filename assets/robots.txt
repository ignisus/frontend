#    .__________________________.
#    | .___________________. |==|
#    | | ................. | |  |
#    | | ::[ Dear robot ]: | |  |
#    | | ::::[ be nice ]:: | |  |
#    | | ::::::::::::::::: | |  |
#    | | ::::::::::::::::: | |  |
#    | | ::::::::::::::::: | |  |
#    | | ::::::::::::::::: | | ,|
#    | !___________________! |(c|
#    !_______________________!__!
#   /                            \
#  /  [][][][][][][][][][][][][]  \
# /  [][][][][][][][][][][][][][]  \
#(  [][][][][____________][][][][]  )
# \ ------------------------------ /
#  \______________________________/

# @@@@@@@   @@@  @@@   @@@@@@   @@@@@@@@  @@@  @@@  @@@  @@@  @@@
# @@@@@@@@  @@@  @@@  @@@@@@@@  @@@@@@@@  @@@@ @@@  @@@  @@@  @@@
# @@!  @@@  @@!  @@@  @@!  @@@  @@!       @@!@!@@@  @@!  @@!  !@@
# !@!  @!@  !@!  @!@  !@!  @!@  !@!       !@!!@!@!  !@!  !@!  @!!
# @!@@!@!   @!@!@!@!  @!@  !@!  @!!!:!    @!@ !!@!  !!@   !@@!@!
# !!@!!!    !!!@!!!!  !@!  !!!  !!!!!:    !@!  !!!  !!!    @!!!
# !!:       !!:  !!!  !!:  !!!  !!:       !!:  !!!  !!:   !: :!!
# :!:       :!:  !:!  :!:  !:!  :!:       :!:  !:!  :!:  :!:  !:!
#  ::       ::   :::  ::::: ::   :: ::::   ::   ::   ::   ::  :::
#  :         :   : :   : :  :   : :: ::   ::    :   :     :   ::
#                 (                           )
#           ) )( (                           ( ) )( (
#        ( ( ( )  ) )                     ( (   (  ) )(
#       ) )     ,,\\\                     ///,,       ) (
#    (  ((    (\\\\//                     \\////)      )
#     ) )    (-(__//                       \\__)-)     (
#    (((   ((-(__||                         ||__)-))    ) )
#   ) )   ((-(-(_||           ```\__        ||_)-)-))   ((
#   ((   ((-(-(/(/\\        ''; 9.- `      //\)\)-)-))    )
#    )   (-(-(/(/(/\\      '';;;;-\~      //\)\)\)-)-)   (   )
# (  (   ((-(-(/(/(/\======,:;:;:;:,======/\)\)\)-)-))   )
#     )  '(((-(/(/(/(//////:%%%%%%%:\\\\\\)\)\)\)-)))`  ( (
#    ((   '((-(/(/(/('uuuu:WWWWWWWWW:uuuu`)\)\)\)-))`    )
#      ))  '((-(/(/(/('|||:wwwwwwwww:|||')\)\)\)-))`    ((
#   (   ((   '((((/(/('uuu:WWWWWWWWW:uuu`)\)\))))`     ))
#         ))   '':::UUUUUU:wwwwwwwww:UUUUUU:::``     ((   )
#           ((      '''''''\uuuuuuuu/``````         ))
#            ))            `JJJJJJJJJ`           ((
#              ((            LLLLLLLLLLL         ))
#                ))         ///|||||||\\\       ((
#                  ))      (/(/(/(^)\)\)\)       ((
#                   ((                           ))
#                     ((                       ((
#                       ( )( ))( ( ( ) )( ) (()

# Don't anger our majestic Fire Phoenix.

# Allow
User-agent: *
Allow: /

# Testing
Disallow: ignisus.org/ia

# Api
Disallow: /api/

Sitemap: http://ignisus.org/sitemap.xml

# AI
User-agent: GPTBot
Disallow: /
User-agent: ChatGPT-User
Disallow: /
User-agent: Google-Extended
Disallow: /
User-agent: PerplexityBot
Disallow: /
User-agent: Amazonbot
Disallow: /
User-agent: ClaudeBot
Disallow: /
User-agent: Omgilibot
Disallow: /
User-Agent: FacebookBot
Disallow: /
User-Agent: Applebot
Disallow: /
User-agent: anthropic-ai
Disallow: /
User-agent: Bytespider
Disallow: /
User-agent: Claude-Web
Disallow: /
User-agent: Diffbot
Disallow: /
User-agent: ImagesiftBot
Disallow: /
User-agent: Omgilibot
Disallow: /
User-agent: Omgili
Disallow: /
User-agent: YouBot
Disallow: /

#              ________
#   __,_,     |        |
#  [_|_/      |   OK   |
#   //        |________|
# _//    __  /
#(_|)   |@@|
# \ \__ \--/ __
#  \o__|----|  |   __
#      \ }{ /\ )_ / _\
#      /\__/\ \__O (__
#     (--/\--)    \__/
#     _)(  )(_
#    `---''---`
